#!/usr/bin/env sh

usage() { echo -e "\nUSAGE:\n$0 [-s : Option. SUBMODULES to checkout. Omit to do all. Pass 0 to do none (for usage with -p) ] [-b : Option. BRANCH to switch to (pass 'PARENT' to switch to branch of same name as current PARENT branch).] [-u : Flag. Track UPSTREAM.] [-f : Flag. Useful for FEATURE branches. Shorthand for '-b PARENT -u'] [-c : Flag. CREATE branch if it doesn't exist.] [-p : Flag. Include PARENT in operation] [-m : Flag. Include to MERGE the previous branches into these ones]" 1>&2; exit 1; }
parentError() { echo -e "\nERROR.\nCannot use -p flag (checkout PARENT) with -b option set to 'PARENT' (or -f flag)."; exit 1; };

startingParentBranch="$(git branch | grep \* | cut -d ' ' -f2)";

doAll=true
while getopts ":s:b:ufcpmh" o; do
    case "${o}" in        
        s)
            submodList=${OPTARG};
            doAll=false
            ;;
        b)
            branch=${OPTARG}
            ;;
        u)
            setUpstream=true
            ;;
        f)
            feature=true
            ;;
        c)
            create=true
            ;;
        p)
            includeParent=true
            ;;
        m)
            mergeLastBranch=true;
            ;;
        h)
            usage
            ;;
    esac
done

#ensure that branch is set, or feature tag is in use
if [ -z "$branch" ]; then
    #check for feature flag
    if [ "$feature" = true ]; then
        branch="PARENT";
        setUpstream=true;
    else 
        usage
    fi
fi

#check for PARENT special value
if [ "$branch" = "PARENT" ]; then
    if [ "$includeParent" = true ]; then
        parentError;
    fi;
    branch="$startingParentBranch";
fi

#check for upstream flag
if [ "$setUpstream" = true ]; then
    upstreamCommands="(git push > /dev/null 2>&1) || (git push --set-upstream origin $branch > /dev/null 2>&1) || git branch -u origin/$branch; ";
fi

#setup command for CREATE option
if [ "$create" = true ]
then
    checkoutCommand="(git checkout $branch > /dev/null 2>&1) || git checkout -b $branch; git fetch; ";
else 
    checkoutCommand="git checkout $branch; ";
fi

submodulemanagmentCommand="git config -f .gitmodules submodule."'$childDir'".branch $branch";
gitCommands="$checkoutCommand""$upstreamCommands""git branch -u origin/$branch $branch;";

#checkout parent first
if [ "$includeParent" = true ]
then
    eval "$gitCommands";    
    affectBranches="PARENT and "
fi

#check for merge flag
if [ "$mergeLastBranch" = true ]
then
    mergeCommand=' git merge -Xignore-space-change --no-ff $childBranch;';
fi


#checkout submodules
if [ "$doAll" = true ]
then
    ./submod/loopAndDo.sh -c "$submodulemanagmentCommand";
    ./submod/loopAndDo.sh -c "$gitCommands""$mergeCommand" -r;
    affectBranches="$affectBranches""all SUBMODULEs."
else 
    ./submod/loopAndDo.sh -s "$submodList" -c "$submodulemanagmentCommand";
    ./submod/loopAndDo.sh -s "$submodList" -c "$gitCommands""$mergeCommand" -r;
    affectBranches="$affectBranches""some SUBMODULEs.";
fi

git add -A && git commit -m "checkout.sh: Set $branch as default submodule branch for $affectBranches";

#check for merge flag AND parent flag
if [ "$mergeLastBranch" = true ] &&  [ "$includeParent" = true ]; then
    git merge -Xignore-space-change --no-ff --no-commit "$startingParentBranch";
#    cycle through mergeIgnore.txt and unstage any mentioned files
    while read ignoreFile; do
        ignoreFile="$(echo -e "${ignoreFile}" | sed -e 's/[[:space:]]*$//')"
        git reset -- $ignoreFile; 
        git checkout -- $ignoreFile;
    done <./mergeIgnore.txt
    git commit
fi
